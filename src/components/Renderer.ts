import {Tile} from './Tile'

export class CanvasRenderer {
    context: CanvasRenderingContext2D = null
    canvasEl: HTMLCanvasElement = null
    canvasWidth = 500
    canvasHeight = 500
    dpiWidth = this.canvasWidth * 2
    dpiHeight = this.canvasHeight * 2

    constructor() {
        this.createCanvas()
    }

    createTile(x: number, y: number, width: number, height: number, state: number) {
        return new Tile(x, y, width, height, state, this.context)
    }

    createCanvas() {
        this.canvasEl = document.createElement('canvas')
        this.canvasEl.setAttribute('id', 'canvas')
        this.canvasEl.style.width = `${this.canvasWidth}px`
        this.canvasEl.style.height = `${this.canvasHeight}px`
        this.canvasEl.style.border = '1px solid #ccc'
        this.canvasEl.width = this.canvasWidth
        this.canvasEl.height = this.canvasHeight
        document.body.appendChild(this.canvasEl)
        this.context = this.getCanvasContext(this.canvasEl)
    }

    getCanvasContext(canvasEl: HTMLCanvasElement) {
        return canvasEl.getContext('2d')
    }
}