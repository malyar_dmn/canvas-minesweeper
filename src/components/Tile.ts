import { ITile } from "../models/ITile";

export class Tile implements ITile {
    x: number;
    y: number;
    width: number;
    height: number;
    context: CanvasRenderingContext2D;
    state: number;
    constructor(
        x: number,
        y: number,
        width: number,
        height: number,
        state: number,
        context: CanvasRenderingContext2D
    ) {
        this.x = x
        this.y = y
        this.width = width
        this.height = height
        this.context = context
        this.state = state

        this.render()
    }
    
    revealTile() {
        if (this.state === 9) {
            this.context.fillStyle = 'red'
            this.context.fillRect(this.x, this.y, this.width, this.height)
        }
        else {
            this.context.fillStyle = '#ccc'
            this.context.fillRect(this.x, this.y, this.width, this.height)
            this.context.stroke()
        }
        // this.renderMinesNumber(this.state)
    }

    setState(state: number) {
        this.state = state
    }

    renderMinesNumber(text: number) {
        this.context.fillStyle = 'black'
        this.context.fillText(String(text), this.x + (this.width / 2), this.y + (this.height / 2))
    }

    render() {
        this.context.rect(this.x, this.y, this.width, this.height)
        this.context.fillStyle = '#ccc'
        this.context.stroke()

    }
}