export interface ITile {
    x: number
    y: number
    width: number
    height: number
    context: CanvasRenderingContext2D
    state: number
    revealTile(): void
    setState(state: number): void
    render(): void
}
