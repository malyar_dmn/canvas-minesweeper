import {CanvasRenderer} from './components/Renderer'
import { ITile } from './models/ITile'

class Game {
    minefield: any[] = []
    minefieldWidth = 10
    minefieldHeight = 10
    minesLeft = 40
    canvas: CanvasRenderer
    constructor() {
        this.canvas = new CanvasRenderer()
        this.createMinefield()
        this.renderMinefield()
        // this.revealAllTiles()
        this.canvas.canvasEl.addEventListener('click', this.handleTileClick.bind(this))
    }

    handleTileClick({offsetX, offsetY}: MouseEvent) {
        console.log('minefield tiles', this.minefield)
        const coords = this.calculateCoords(offsetX, offsetY)
        if (this.minefield[coords.y][coords.x].state === 9) {
            this.minefield[coords.y][coords.x].revealTile()
            console.log('end game')
        }
        this.checkNeighbourBomb(coords)
        this.minefield[coords.y][coords.x].revealTile()
    }

    revealAllTiles() {
        this.minefield.forEach(row => {
            row.forEach((col: ITile) => {
                col.revealTile()
            })
        })
    }


    calculateCoords(mouseX: number, mouseY: number) {
        const x = Math.ceil((mouseX / this.canvas.canvasWidth) * this.minefieldWidth) - 1
        const y = Math.ceil((mouseY / this.canvas.canvasHeight) * this.minefieldHeight) - 1

    
        return {x, y}
    }

    checkNeighbourBomb(tileCoord: any) {
        let bombsAroundTile = 0

        for (let j = -1; j < 2; j++) {
            for (let i = -1; i < 2; i++) {
                if (j === 0 && i === 0) continue
                const row = this.minefield[tileCoord.y + j]
                if (!row) continue
                const cell: ITile = row[tileCoord.x + i]
                if (!cell) continue
                if (cell.state === 9) {
                    bombsAroundTile++
                }
                if (cell.state === 0) {
                    cell.revealTile()
                }
                console.log(cell)
            }
        }
        console.log('bombs around', bombsAroundTile)
        // console.log('selected tile', this.minefield[tileCoord.x][tileCoord.y])
        this.minefield[tileCoord.y][tileCoord.x].setState(bombsAroundTile)
    }

    createMinesArray() {
        return new Array(this.minesLeft).fill(9)
    }

    convertArrayTo2D(arr: any[], colElementsAmount: number) {
        for (let i = 0; i < arr.length; i++) {
            this.minefield.push(arr.splice(0, colElementsAmount))
        }
    }
    
    createMinefield() {
        const minesArr: number[] = this.createMinesArray()
        const emptyArray = new Array(this.minefieldWidth * this.minefieldHeight - this.minesLeft).fill(0)
        const gameArray = emptyArray.concat(minesArr)
        const shuffledArray = gameArray.sort(() => Math.random() -0.5)

        this.convertArrayTo2D(shuffledArray, this.minefieldWidth)
        console.log(this.minefield)
    }

    renderMinefield() {
        for (let i = 0; i < this.minefieldHeight; i++) {
            for (let j = 0; j < this.minefieldWidth; j++) {
                const tile = this.canvas.createTile((this.canvas.canvasWidth / this.minefieldWidth) * j, (this.canvas.canvasHeight / this.minefieldHeight) * i, 50, 50, this.minefield[i][j])
                this.minefield[i][j] = tile
            }
        }

        console.log(this.minefield)
    }

    // createMinefield() {
    //     for (let i = 0; i < this.minefieldHeight; i++) {
    //         this.minefield[i] = new Array(this.minefieldHeight)
    //         for (let j = 0; j < this.minefieldWidth; j++) {
    //             const rand = Math.random() < 0.5
    //             const tile = this.canvas.createTile((this.canvas.canvasWidth / this.minefieldWidth) * j, (this.canvas.canvasHeight / this.minefieldHeight) * i, 50, 50)             
    //             this.minefield[i][j] = tile
    //         }
    //     }
    // }

}


const game = new Game()
